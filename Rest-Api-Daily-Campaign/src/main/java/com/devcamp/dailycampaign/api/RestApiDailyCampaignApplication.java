package com.devcamp.dailycampaign.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiDailyCampaignApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiDailyCampaignApplication.class, args);
	}

}
