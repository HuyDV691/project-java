package com.devcamp.dailycampaign.api;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CDailyCampaign {
	@CrossOrigin
	@GetMapping("/devcamp-campaign")	
	public String getDateViet() {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
		LocalDate today = LocalDate.now(ZoneId.systemDefault());
		
		
		String vDayOfWeek = dtfVietnam.format(today);
		String vDayCampaign = "";
		switch (vDayOfWeek) {
		case "Thứ Hai":
			vDayCampaign = String.format("%s, mua 1 tặng 1.", vDayOfWeek);
			break;
		case "Thứ Ba":
			vDayCampaign = String.format("%s, tặng tất cả khách hàng một phần bánh ngọt", vDayOfWeek);
			break;
		case "Thứ Tư":
			vDayCampaign = String.format("%s, tặng tất cả khách hàng một phần nước uống.", vDayOfWeek);
			break;
		case "Thứ Năm":
			vDayCampaign = String.format("%s, giảm giá 10% cho mọi đơn hàng.", vDayOfWeek);
			break;
		case "Thứ Sáu":
			vDayCampaign = String.format("%s, tặng tất cả khách hàng một phần salad.", vDayOfWeek);
			break;
		case "Thứ Bảy":
			vDayCampaign = String.format("%s, mua 1 tặng 1 size nhỏ hơn.", vDayOfWeek);
			break;
		case "Chủ Nhật":
			vDayCampaign = String.format("%s, đồng giá size M và L.", vDayOfWeek);
			break;
		}
		return vDayCampaign;
	}
}
