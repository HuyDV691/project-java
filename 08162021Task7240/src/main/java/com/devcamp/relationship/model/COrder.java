package com.devcamp.relationship.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class COrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_created")
	private Date createdAt;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer", nullable = false)
	private CCustomer customer;

	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			mappedBy = "order")
	private CPayment payment;

	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {
					CascadeType.PERSIST,
					CascadeType.MERGE
			})
	@JoinTable(name = "order_product",
			joinColumns = {@JoinColumn(name = "order_id")},
			inverseJoinColumns = {@JoinColumn(name = "product_id")})
	private Set<CProduct> products;
	
	public COrder() {
		super();
		// TODO Auto-generated constructor stub
	}

	public COrder(long id, Date createdAt, CCustomer customer, CPayment payment, Set<CProduct> products) {
		super();
		this.id = id;
		this.createdAt = createdAt;
		this.customer = customer;
		this.payment = payment;
		this.products = products;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public CCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CCustomer customer) {
		this.customer = customer;
	}

	public CPayment getPayment() {
		return payment;
	}

	public void setPayment(CPayment payment) {
		this.payment = payment;
	}

	public Set<CProduct> getProducts() {
		return products;
	}

	public void setProducts(Set<CProduct> products) {
		this.products = products;
	}
}
