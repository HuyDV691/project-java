-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2021 at 01:04 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `midexam_class-student`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(22);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) NOT NULL,
  `student_adress` varchar(255) DEFAULT NULL,
  `student_birthday` date DEFAULT NULL,
  `student_code` varchar(255) DEFAULT NULL,
  `student_name` varchar(255) DEFAULT NULL,
  `student_phone` bigint(20) DEFAULT NULL,
  `student_sex` varchar(255) DEFAULT NULL,
  `table_class_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `student_adress`, `student_birthday`, `student_code`, `student_name`, `student_phone`, `student_sex`, `table_class_id`) VALUES
(2, 'std address 2', '2021-08-04', 'stdcode02', 'std name 02', 987654322, 'male', 1),
(3, 'std address 3', '2021-08-05', 'stdcode03', 'std name 03', 987654323, 'female', 1),
(4, 'std address 1', '2021-08-03', 'testupdateStudent4', 'std name 01', 984321, 'male', 2),
(5, 'std address 5', '2021-08-07', 'stdcode05', 'std name 05', 987654325, 'female', 2),
(15, 'sadads', '2121-12-12', 'asd', 'asdadasdad', 123, 'adads', 2),
(16, 'sadads', '1212-12-12', 'asd23', 'asdadasdad', 1234, 'adads', 3);

-- --------------------------------------------------------

--
-- Table structure for table `table_class`
--

CREATE TABLE `table_class` (
  `id` bigint(20) NOT NULL,
  `class_code` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `teacher_name` varchar(255) DEFAULT NULL,
  `teacher_phone` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_class`
--

INSERT INTO `table_class` (`id`, `class_code`, `class_name`, `teacher_name`, `teacher_phone`) VALUES
(1, 'Class001', 'name Class 1', 'teacher name', 987654321),
(2, 'Class002', 'name Class 2', 'teacher name 02', 987654321),
(3, 'Class003', 'name Class 3', 'teacher name 03', 987654321),
(21, 'Class004', 'class number 4', 'teacher 4', 98765);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_cgcf3r5xk73o0etbduc1qxnol` (`student_code`),
  ADD UNIQUE KEY `UK_nhpjww3t6jwsuxq0lixv4nf27` (`student_phone`),
  ADD KEY `FK3lph006vypumjbdyp2f7qkdx9` (`table_class_id`);

--
-- Indexes for table `table_class`
--
ALTER TABLE `table_class`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_drh1kvnbssk6bdcicwwsabpmk` (`class_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `FK3lph006vypumjbdyp2f7qkdx9` FOREIGN KEY (`table_class_id`) REFERENCES `table_class` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
