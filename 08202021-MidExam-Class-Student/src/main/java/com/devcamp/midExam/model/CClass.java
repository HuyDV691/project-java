package com.devcamp.midExam.model;

import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "table_class")
public class CClass {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotEmpty(message = "chua nhap class code")
	@Column(name = "class_code", unique = true )
	private String classCode;
	
	@NotEmpty(message = "chua nhap class name")
	@Column(name = "class_name")
	private String className;
	
	@NotEmpty(message = "chua nhap teacher name")
	@Column(name = "teacher_name")
	private String teacherName;
	
	@NotNull(message = "chua nhap teacher phone")
	@Column(name = "teacher_phone")
	private Long teacherPhone;
	
	@OneToMany(targetEntity = CStudent.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "table_class_id")
	@JsonIgnore
	private List<CStudent> students;

	public CClass() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CClass(Long id, String classCode, String className, String teacherName, Long teacherPhone,
			List<CStudent> students) {
		super();
		this.id = id;
		this.classCode = classCode;
		this.className = className;
		this.teacherName = teacherName;
		this.teacherPhone = teacherPhone;
		this.students = students;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Long getTeacherPhone() {
		return teacherPhone;
	}

	public void setTeacherPhone(Long teacherPhone) {
		this.teacherPhone = teacherPhone;
	}

	@JsonIgnore
	public List<CStudent> getStudents() {
		return students;
	}

	public void setStudents(List<CStudent> students) {
		this.students = students;
	}
}
