package com.devcamp.midExam.model;

import java.sql.Date;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "students")
public class CStudent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "chua nhap student code")
	@Column(name = "student_code", unique = true)
	private String studentCode;

	@NotEmpty(message = "chua nhap student name")
	@Column(name = "student_name")
	private String studentName;

	@NotEmpty(message = "chua nhap gioi tinh")
	@Column(name = "student_sex")
	private String studentSex;

	@NotNull(message = "chua nhap ngay sinh")
	@Column(name = "student_birthday")
	private Date studentBirthDay;

	@NotEmpty(message = "chua nhap dia chi")
	@Column(name = "student_adress")
	private String studentAdress;

	@NotNull(message = "chua nhap student phone")
	@Column(name = "student_phone", unique = true)
	private Long studentPhone;

	@ManyToOne
	//@JsonIgnore
	private CClass tableClass;

	@Transient
	private String classCode;
	
	public CStudent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CStudent(Long id, String studentCode, String studentName, String studentSex, Date studentBirthDay,
			String studentAdress, Long studentPhone, CClass className) {
		super();
		this.id = id;
		this.studentCode = studentCode;
		this.studentName = studentName;
		this.studentSex = studentSex;
		this.studentBirthDay = studentBirthDay;
		this.studentAdress = studentAdress;
		this.studentPhone = studentPhone;
		this.tableClass = className;
	}

	
	//@JsonIgnore
	public String getClassCode() {
		return getTableClass().getClassCode();
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStudentCode() {
		return studentCode;
	}

	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentSex() {
		return studentSex;
	}

	public void setStudentSex(String studentSex) {
		this.studentSex = studentSex;
	}

	public Date getStudentBirthDay() {
		return studentBirthDay;
	}

	public void setStudentBirthDay(Date studentBirthDay) {
		this.studentBirthDay = studentBirthDay;
	}

	public String getStudentAdress() {
		return studentAdress;
	}

	public void setStudentAdress(String studentAdress) {
		this.studentAdress = studentAdress;
	}

	public Long getStudentPhone() {
		return studentPhone;
	}

	public void setStudentPhone(Long studentPhone) {
		this.studentPhone = studentPhone;
	}

	//@JsonIgnore
	public CClass getTableClass() {
		return tableClass;
	}

	public void setTableClass(CClass tableClass) {
		this.tableClass = tableClass;
	}
	
	

}
