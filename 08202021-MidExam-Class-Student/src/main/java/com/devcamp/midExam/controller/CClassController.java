package com.devcamp.midExam.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.midExam.model.CClass;
import com.devcamp.midExam.repository.IClassRepository;

@RestController
public class CClassController {

	@Autowired
	private IClassRepository classRespository;

	// Get all info class
	@CrossOrigin
	@GetMapping("/classes")
	public ResponseEntity<List<CClass>> getAllClass() {
		try {
			List<CClass> listAllClass = new ArrayList<CClass>();
			classRespository.findAll().forEach(listAllClass::add);
			return new ResponseEntity<>(listAllClass, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get info class by Id
	@CrossOrigin
	@GetMapping("/classes/{classId}")
	public CClass getClassInfoByClassId(@PathVariable Long classId) {
		if (classRespository.findById(classId).isPresent())
			return classRespository.findById(classId).get();
		else
			return null;
	}

	// Delete Class by Id
	@CrossOrigin
	@DeleteMapping("/classes/{classId}")
	public ResponseEntity<Object> deleteClassById(@PathVariable Long classId) {
		try {
			classRespository.deleteById(classId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All Class
	@CrossOrigin
	@DeleteMapping("/classes")
	public ResponseEntity<CClass> deleteAllClass() {
		try {
			classRespository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Create new Class
	@CrossOrigin
	@PostMapping("/classes")
	public ResponseEntity<Object> createNewClass(@Valid @RequestBody CClass pClass) {
		try {
			CClass newClass = new CClass();
			newClass.setClassCode(pClass.getClassCode());
			newClass.setClassName(pClass.getClassName());
			newClass.setTeacherName(pClass.getTeacherName());
			newClass.setTeacherPhone(pClass.getTeacherPhone());
			newClass.setStudents(pClass.getStudents());

			CClass saveNewClass = classRespository.save(newClass);
			return new ResponseEntity<>(saveNewClass, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create New Class: " + e.getCause().getCause().getMessage());
		}
	}

	// Update Class Exist
	@CrossOrigin
	@PutMapping("/classes/{classId}")
	public ResponseEntity<Object> updateClass(@Valid @PathVariable("classId") Long classId, 
			@RequestBody CClass pClass){
		Optional<CClass> existClass = classRespository.findById(classId);
		if (existClass.isPresent()) {
			CClass updateClass = existClass.get();
			updateClass.setClassCode(pClass.getClassCode());
			updateClass.setClassName(pClass.getClassName());
			updateClass.setTeacherName(pClass.getTeacherName());
			updateClass.setTeacherPhone(pClass.getTeacherPhone());
			updateClass.setStudents(pClass.getStudents());
			
			
			CClass saveUpdateClass = classRespository.save(updateClass);
			return new ResponseEntity<>(saveUpdateClass, HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Khong ton tai Class de update",HttpStatus.NOT_FOUND);
		}
	}
}
