package com.devcamp.midExam.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.midExam.model.CClass;
import com.devcamp.midExam.model.CStudent;
import com.devcamp.midExam.repository.IClassRepository;
import com.devcamp.midExam.repository.IStudentRepository;

@RestController
public class CStudentController {

	@Autowired
	private IStudentRepository studentRespository;
	@Autowired
	private IClassRepository classRepository;

	// Get all info Student
	@CrossOrigin
	@GetMapping("/students")
	public ResponseEntity<List<CStudent>> getAllStudent() {
		try {
			List<CStudent> listAllStudent = new ArrayList<CStudent>();
			studentRespository.findAll().forEach(listAllStudent::add);
			return new ResponseEntity<>(listAllStudent, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get all info Student by classId
	@CrossOrigin
	@GetMapping("/classes/{classId}/students")
	public List<CStudent> getListStudentByClassId(@PathVariable(value = "classId") Long classId) {
		return studentRespository.findByTableClassId(classId);
	}

	// Get info Student by StudentId
	@CrossOrigin
	@GetMapping("/students/{studentId}")
	public CStudent getStudentByStudentId(@PathVariable Long studentId) {
		if (studentRespository.findById(studentId).isPresent())
			return studentRespository.findById(studentId).get();
		else
			return null;
	}

	// Delete Student by StudentId
	@CrossOrigin
	@DeleteMapping("/students/{studentId}")
	public ResponseEntity<Object> deleteStudentByStudentId(@PathVariable(value = "studentId") Long studentId) {
		try {
			studentRespository.deleteById(studentId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Delete All Student
	@CrossOrigin
	@DeleteMapping("/students")
	public ResponseEntity<CStudent> deleteAllStudent() {
		try {
			studentRespository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Create new Student
	@CrossOrigin
	@PostMapping("/classes/{classId}/students")
	public ResponseEntity<Object> createNewStudent(@Valid @PathVariable("classId") Long classId,
			@RequestBody CStudent pStudent) {
		try {
			Optional<CClass> classData = classRepository.findById(classId);
			if (classData.isPresent()) {
				CStudent newStudent = new CStudent();
				newStudent.setStudentCode(pStudent.getStudentCode());
				newStudent.setStudentName(pStudent.getStudentName());
				newStudent.setStudentSex(pStudent.getStudentSex());
				newStudent.setStudentBirthDay(pStudent.getStudentBirthDay());
				newStudent.setStudentAdress(pStudent.getStudentAdress());
				newStudent.setStudentPhone(pStudent.getStudentPhone());
				newStudent.setTableClass(pStudent.getTableClass());

				CClass _class = classData.get();
				newStudent.setTableClass(_class);
				newStudent.setClassCode(_class.getClassCode());

				CStudent saveNewStudent = studentRespository.save(newStudent);
				return new ResponseEntity<>(saveNewStudent, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("Class Id khong ton tai", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Student: " + e.getCause().getCause().getMessage());
		}
	}

	// Update Student
	@CrossOrigin
	@PutMapping("/classes/{classId}/students/{studentId}")
	public ResponseEntity<Object> updateStudent(@Valid @PathVariable("classId") Long classId,
			@PathVariable("studentId") Long studentId, @RequestBody CStudent pstudent) {
		try {
			Optional<CStudent> studentIdExistData = studentRespository.findById(studentId);
			Optional<CClass> classIdExistData = classRepository.findById(classId);
			if (studentIdExistData.isPresent() & classIdExistData.isPresent()) {
				CStudent updateStudent = studentIdExistData.get();
				updateStudent.setStudentCode(pstudent.getStudentCode());
				updateStudent.setStudentName(pstudent.getStudentName());
				updateStudent.setStudentSex(pstudent.getStudentSex());
				updateStudent.setStudentBirthDay(pstudent.getStudentBirthDay());
				updateStudent.setStudentAdress(pstudent.getStudentAdress());
				updateStudent.setStudentPhone(pstudent.getStudentPhone());

				CStudent saveUpdateStudent = studentRespository.save(updateStudent);
				return new ResponseEntity<>(saveUpdateStudent, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Cap nhat khong thanh cong do khong ton tai ClassId or Student Id",
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}
}
