package com.devcamp.midExam.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.midExam.model.CClass;

public interface IClassRepository extends JpaRepository<CClass, Long> {
	CClass findByClassCode(String classCode);
}
