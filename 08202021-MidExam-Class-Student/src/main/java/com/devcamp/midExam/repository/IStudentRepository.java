package com.devcamp.midExam.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.midExam.model.CStudent;

public interface IStudentRepository extends JpaRepository<CStudent, Long> {
	List<CStudent> findByTableClassId(Long id);
}
