package com.devcamp.country_regionCRUD.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.country_regionCRUD.model.CCountry;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	List<CCountry> findByCountryCode(String countryCode);
	
	List<CCountry> findByCountryName(String countryName);
	
	
}
