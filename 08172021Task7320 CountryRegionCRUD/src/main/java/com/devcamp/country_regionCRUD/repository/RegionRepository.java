package com.devcamp.country_regionCRUD.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.country_regionCRUD.model.CRegion;

@Repository
public interface RegionRepository extends JpaRepository<CRegion, Long> {
	 List<CRegion> findByCountryId(Long countryId);
	 
	 List<CRegion> findByRegionCode(String regionCode);
	 List<CRegion> findByRegionName(String regionName);
	 Optional<CRegion> findByIdAndCountryId(Long id, Long instructorId);
}

